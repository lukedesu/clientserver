/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package clientserver;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;

/**
 *
 * @author Gianluca Suriani
 */
public class Server {
    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        System.out.println("== Server ==");
        System.out.print("Waiting for connection...\r");
        ServerSocket serverSocket = new ServerSocket(2000);
        Socket socket =serverSocket.accept();

        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        writer.write("👋 Benvenuto!\n");
        writer.flush();
        
        
        Long date = new Date().getTime();

        
        String clientMessage = br.readLine();
        if(clientMessage.equals("sync")){
            System.out.println("Received Sync from client!");
            writer.write(date.toString());
            writer.flush();
        }
        
        
        System.out.println("Sending date: " + date);
        socket.close();        
    }    
}
