/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package clientserver;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.Date;

/**
 *
 * @author Gianluca Suriani
 */
public class Client {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        try{
            System.out.println("== Client ==");
            Socket socket = new Socket("127.0.0.1", 2000);
            BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            
            String msg = br.readLine();
            System.out.println("Hello server!\n");
        
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            writer.write("sync\n");
            writer.flush();
            
            
            msg = br.readLine();
            Date d = new Date(Long.parseLong(msg));
            System.out.println(d.toString());
        
        
            socket.close();
        } catch(IOException e){
            System.err.println("Could not connect to the server!\n" + e);
        }
    }
    
}
